# jruzicka's presentation slides

All presentations are in [marp](https://marp.app/) Markdown.

To run a `marp` server for all slides:

```
marp -s src
```

Render HTML and PDF:

```
./render.sh
```
