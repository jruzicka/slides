---
marp: true
author: Jakub Ružička
url: https://pkg.labs.nic.cz/pages/apkg/
---

# run CLI tools with python `asyncio` 🐍

source: [https://gitlab.nic.cz/jruzicka/slides](https://gitlab.nic.cz/jruzicka/slides)

---

# use case - automation 🤖

[apkg](https://pkg.labs.nic.cz/pages/apkg/) the cross-distro packaging automation tool needs to run various system-specific CLI tools such as `dpkg` or `rpmbuild`.

`stdout` and `stderr` need to be captured and displayed at the same time.


---

# `subprocess.Popen()`

* the original way to run processes
* available since Python 3.0, mostly usable since Python 3.3
* can't `tee`

```python
prc = subprocess.Popen(
    cmd, shell=shell,
    stdin=stdin, stdout=stdout, stderr=stderr,
    env=env)

out, err = prc.communicate(input=input)
```

---

# `subprocess.run()`

* the new default way to run processes
* convenient `Popen` wrapper with `CompletedProcess` output
* introduced in Python 3.5

```python
out = subprocess.run(
    cmd, shell=shell,
    stdin=stdin, stdout=stdout, stderr=stderr,
    input=input, env=env)
```

---

# honorable mention: `sh` library

[sh](https://amoffat.github.io/sh/) is a full-fledged subprocess replacement
that allows you to call any program as if it were a function:


```python
from sh import ifconfig
print(ifconfig("wlan0"))
```

* can't `tee`

---

# `asyncio`

includes coroutines to run a process asynchronously:


```python
asyncio.create_subprocess_exec(program, *args,
    stdin=None, stdout=None, stderr=None, limit=None, **kwds)¶
```

and

```python
asyncio.create_subprocess_shell(cmd,
    stdin=None, stdout=None, stderr=None, limit=None, **kwds)
```

* available since Python 3.5 (?), safe to use sice 3.6

---

# `asyncio` official example

```python
import asyncio

async def run(cmd):
    proc = await asyncio.create_subprocess_shell(
        cmd,
        stdout=asyncio.subprocess.PIPE,
        stderr=asyncio.subprocess.PIPE)

    stdout, stderr = await proc.communicate()

    print(f'[{cmd!r} exited with {proc.returncode}]')
    if stdout:
        print(f'[stdout]\n{stdout.decode()}')
    if stderr:
        print(f'[stderr]\n{stderr.decode()}')

asyncio.run(run('ls /zzz'))
```

---

# `subprocess_tee` library

[subprocess_tee](https://github.com/pycontribs/subprocess-tee) is drop-in alternative to `subprocess.run`
that captures the output while still printing it in real-time,
just the way `tee` does.


```python
# from subprocess import run
from subprocess_tee import run

result = run("echo 123")
result.stdout == "123\n"
```

* doesn't pass all arguments to `asyncio.create_subprocess_shell()`

---

# have some `tee`

It's time to reinvent a 30 years old wheel 🛞

```python
async def _tee(*args, shell=False, **kwargs):
    """
    async version of subprocess.run() which can both
    stream and capture stdout/stderr like unix tee

    Print both stdout/stderr to stderr in order not to polute
    stdout with random command output.

    Use run() function from this module with tee=True
    to use this in a convenient way.
    """
```

---

```python
if shell:
    process = await asyncio.create_subprocess_shell(
        args[0],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        **kwargs
    )
else:
    process = await asyncio.create_subprocess_exec(
        *args,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        **kwargs
    )
```

* notice different `args` passing
* can't pass `input` arg for `communicate()`

---

```python
    out = []
    err = []

    def tee_fun(line, sink):
        line_str = line.decode("utf-8").rstrip()
        sink.append(line_str)
        print(line_str, file=sys.stderr)
```

* `tee_fun()` is a callback for storing a line into a sink and printing it
* `apkg` redirects both `stdout` and `stderr` to `stderr` in order to keep
  `stdout` clean for output

---

```python
    loop = asyncio.get_event_loop_policy().get_event_loop()
    tasks = []

    if process.stdout:
        tasks.append(loop.create_task(_read_stream(
            process.stdout, lambda l: tee_fun(l, out))))

    if process.stderr:
        tasks.append(loop.create_task(_read_stream(
            process.stderr, lambda l: tee_fun(l, err))))

    await asyncio.wait(set(tasks))
```

* `_read_stream()` simply awaits `stream.readline()`
  and calls the supplied callback function

---

```python
    stdout = os.linesep.join(out) + os.linesep
    stderr = os.linesep.join(err) + os.linesep

    return subprocess.CompletedProcess(
        args=list(args),
        returncode=await process.wait(),
        stdout=stdout,
        stderr=stderr,
    )
```

* return `CompletedProcess` object like `subprocess.run()`

---

# `run()` with `tee` support

```python
if tee:
    try:
        loop = asyncio.get_event_loop_policy().get_event_loop()
        result = loop.run_until_complete(_tee(*cmd, **kwargs))
    except FileNotFoundError:
        raise ex.CommandNotFound(cmd=cmd_str)
else:
    try:
        result = subprocess.run(
            cmd,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            check=False,
            universal_newlines=True,
            **kwargs)
    except OSError:
        raise ex.CommandNotFound(cmd=cmd_str)
```

* full source in [apkg.util.run](https://gitlab.nic.cz/packaging/apkg/-/blob/master/apkg/util/run.py)

---

# `run()` it 🏃🏽

```python
from apkg.util.run import run

out = run('echo', '-e', 'Hello World!')
# or
out = run(['echo', '-e', 'Hello World!'])
# or
out = run('echo -e "Hello World!"', shell=True)

assert out == "Hello World!"
assert out.stderr == ""
assert out.returncode == 0
```
[apkg]: https://pkg.labs.nic.cz/pages/apkg/
